# YAOOK/bare-metal Handbook

This documentation is generated using sphinx.

## Table of Contents

See [index.md](index.md).

## How to render

Install sphinx by executing:

```shell
# Install dependencies with pip
pip install sphinx
```

To build the documentation use:

```shell
# Build documentation
python3 -m sphinx docs _build/html
# Open in Firefox
firefox _build/html/index.html
```
