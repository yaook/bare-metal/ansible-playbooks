---
hide-doc: true
---

# Welcome to YAOOK/bare-metal's documentation!
```{eval-rst}
.. toctree::
    :hidden:

    Releases <https://gitlab.com/yaook/bare-metal/ansible-playbooks/-/releases>
    releasenotes

.. toctree::
    :hidden:
    :caption: Developer Documentation

    developer/explanation/index
    developer/guide/index

```

::::{grid} 1
:::{grid-item-card}  Releasenotes
:link: releasenotes
:link-type: doc
The releasenotes give you all essential information about recent changes.
:::
::::

---

::::{grid} 2
:::{grid-item-card}  Developer Explanations
:link: /developer/explanation/index
:link-type: doc
In-depth explanations and discussion about how (and why) YAOOK/bare-metal works from the developer perspective.
:::
:::{grid-item-card}  Developer Guides
:link: /developer/guide/index
:link-type: doc
Keep this at hand when *developing* with YAOOK/bare-metal.
:::
::::
