# Developer Guides

```{toctree}
---
maxdepth: 3
hidden: true
---

coding-guide
```

Keep this at hand when *developing* with YAOOK/bare-metal.


::::{grid} 2
:::{grid-item-card} Coding Guide
:link: /developer/guide/coding-guide
:link-type: doc
:::
::::

