Coding Guide
============
This document contains a coding guideline specifically written for this
repository. For general information please refer to the
`YAOOK Development Process Documentation <https://yaook.gitlab.io/meta/01-development-process.html>`__.

.. _coding-guide.towncrier:

Creation of release notes
-------------------------

The changelog/releasenotes is a place, where a user can see, what has changed.
It's a first reference where to look when e.g. something no longer works.
So the important information which needs to be given here is ``what`` has changed.
Not ``why`` or ``how``. These are informations which can be found in the history.
As a developer try to keep it short (see
`keepachangelog <https://keepachangelog.com/en/1.1.0/>`__) and provide
further information in the related issue/MR.

.. attention::

   No direct editing of the CHANGELOG-file!

We use `towncrier <https://github.com/twisted/towncrier>`__ for
the management of our release notes. Therefore developers must adhere to some
conventions.

For every MR you need to place a file called
``<merge-request-ID>.<type>[.<whatever-you-want>]`` into ``/docs/_releasenotes``.
The content of the file is the actual release note and is in reStructuredText format.

The ``merge-request-ID`` will automatically be added/corrected for you.
So if you don't know the ``merge-request-ID`` in advance, just type anything
instead of the ID. Please provide the file in your last commit as the pipeline will
``git commit --amend`` and ``git push --force`` the corrected filename back to
your branch. Don't forget to ``git pull --rebase=true`` afterwards, if you make new changes.

.. note::

   When you are working in a fork the file won't be changed, but the pipeline will
   fail. Please edit the file manually.

.. note::

   Sometimes the pipeline fails with ``RuntimeError: No releasenote file added.
   Make sure to provide a file with your MR.`` If you provided a note it's likely
   that you have to rebase to ``origin/devel`` to make the pipeline pass.

**Currently we use the following types:**

.. table::

   ============================= ===================================
   type                          description
   ============================= ===================================
   ``BREAKING``                  anything which requires manual intervention
                                 of users or changes the behavior of end
                                 resources or workload
   ``feature``                   new feature introduced
   ``change``                    old functionality changed/updated
   ``fix``                       any bugfixes
   ``removal``                   soon-to-be-removed and removed features
   ``docs``                      any changes in the documentation
   ``security``                  security patches
   ``chore``                     updating grunt tasks etc, no production
                                 code change, non-user-facing-changes e.g.

                                 - configuration changes (like .gitignore)
                                 - private methods
                                 - update dependencies
                                 - refactoring

   ``misc``                      everything not needing a description and
                                 not of interest to users
                                 (even if there is content it's not written
                                 in the releasenotes-file)
   ============================= ===================================

.. note::

   For **breaking changes** please provide detailed information on what needs to be done
   as an operator.
   Either in the releasenote itself or linking inside the note to some other source
   (e.g. parts in our docs, ..)

**Nothing to report in the releasenotes**

   leave your file empty, this will just leave a link to the corresponding MR.

**Really nothing to add to releasenotes**

   if you just correct a typo or something which really no user cares,
   name your file ``+.misc.<random>``, this will not provide an entry in the releasenotes
   (and no link to a MR)

So the following file-names would be valid examples:

.. code:: none

   123.feature
   12ads3.feature.addedsomethingdifferent
   12.docs.rst
   +.misc.jkdfskjhsfd2
   something.chore.rst
   99.BREAKING.rst
   100.BREAKING.idestroyedeverything.rst
   käsekuchen.fix.istlecker

The content in the file can be formated using rst-syntax. Headlines are not allowed.
For further informations see the
`towncrier docu <https://towncrier.readthedocs.io/en/stable/tutorial.html#creating-news-fragments>`__.

.. _coding-guide.releases:

If you are responsible for the creation of releases
---------------------------------------------------

**How to trigger a release:**

1. Go to `rdm <https://gitlab.com/yaook/rdm>`__ and start a pipeline setting ``YAOOK_BM_CI_RELEASE`` to ``true``.
2. After a few minutes there should be a new ``release-prepare/v$Major.$Minor.$Patch``-branch.
3. The pipeline is triggered like described in :ref:`the policy <release-and-versioning-policy.yaook-bare-metal-implementation>`
4. Make sure the pipeline did pass sucessfully and especially the changelog is rendered correctly, otherwise correct it directly on
   the branch, this will start a new pipeline.
5. If you for whatever reason don't need the predefined timeperiod before the release candidate will become a release,
   manually start the delayed ``merge-to-release-branch``-job.

**What not to do**

- Don't change anything on the ``release-prepare/v$Major.$Minor.$Patch`` branch, after it was merged to the
   ``main``-branch. If you see an error or something which needs to be fixed,
   do it before the ``merge-to-release-branch``-job has started or on ``devel`` for the next release.
