Releases and Versioning
=======================

In order to ensure that we do not ship broken things we need to test all changes before releasing them.

Requirements
------------

* Guarantee stability by testing the entire system before releasing it [t.b.d]
* Release new features and fixes in a regular and fast way
* Be able to fix critical bugs/security issues within a few hours while not sacrificing stability

Versioning Overview
-------------------

YAOOK/bare-metal is developed and versioned according to cycles. Each development cycle has a predefined
amount of to-be-implemented features (but can of course have additional features and fixes implemented).
During each cycle contributors can merge changes to the main branch (``devel``) of the repository.

When the cycle ends all changes on the main branch are pushed to a ``release-prepare/v$Major.$Minor.$Patch`` branch.
The goal of the ``release-prepare/v$Major.$Minor.$Patch`` branch is to have a chance to test changes in a stable and controlled way. [t.b.d.]
Ideally no changes are made to the ``release-prepare/v$Major.$Minor.$Patch`` branch anymore. Only necessary hotfixes are allowed.

The ``release-prepare/v$Major.$Minor.$Patch`` branch is kept like that for a week at which point it merges to the
release-branch ``main`` and gets a final version number.


Detailed branching model
************************

The repository is structured in four branch-types:

``devel``
    The main working branch of the repository. Any change is first merged in here.
    Code in here can be expected to pass all linting as well as very basic functionality tests.
    When developing on YAOOK/bare-metal this should be your base branch.
    You should not run any useful deployment from here.
    This branch is a long-living branch.

``feature``
    All changes are developed in feature or fix-branches (the name is free), branching off and merging
    back to devel and therefore passing the beforementioned tests.

``release-prepare/v$Major.$Minor.$Patch``
    These branches are mainly used for release management tasks.

``main``
    We have one branch containing all releases. The ``release-prepare/v$Major.$Minor.$Patch``-branch is merged into it.
    This branch is a long-living branch.

.. figure:: /img/release.svg
   :scale: 100%
   :alt: branching-model
   :align: center

.. _release-and-versioning-policy.versioning-specification:

Versioning specification
************************

We define the following versioning scheme following the SemVer concept (https://semver.org/) ``$Major.$Minor.$Patch``:

* We increment ``$Major`` by 1 if we have an incompatible change.
* We increment ``$Minor`` by 1 every time we added at least one new feature in the release. It starts from 0 for each major release.
* We increment ``$Patch`` by 1 for every release not introducing a new feature. It starts from 0 for each normal release.


.. _release-and-versioning-policy.yaook-bare-metal-implementation:

YAOOK/bare-metal implementation
*******************************

The following describes the practical implementation of these concepts.

The release pipeline of the YAOOK/bare-metal repository is following these steps:
    - create a ``release-prepare/v$Major.$Minor.$Patch``-branch based off ``devel``
    - on the branch do the following:
        - calculate the next version number based on the provided releasenote-files since the last release
          and write it to the version-file
        - generate the changelog using towncrier and remove the old releasenote-files

The pipeline for the ``release-prepare/v$Major.$Minor.$Patch``-branch does the following:
    - run all tests (currently only some basic linting)
    - tag the commit with ``v$Major.$Minor.$Patch-rc-<build-nr>`` if it's a major or minor release
      (currently disabled)
    - create a delayed job (one week) which
        - merges to ``main``
        - triggers a MR back to ``devel``

The pipeline for the ``main``-branch
    - does again some basic linting (depending on changes),
    - tags the release with ``v$Major.$Minor.$Patch`` and
    - creates a `Gitlab Release <https://docs.gitlab.com/ee/user/project/releases/>`__.

The pipeline on ``devel`` does - besides other things - create and publish the documentation.
