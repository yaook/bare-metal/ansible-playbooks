# Developer Explanations

```{toctree}
---
maxdepth: 3
hidden: true
---

release-and-versioning-policy
```

In-depth explanations and discussion about how (and why) YAOOK/bare-metal works from the developer perspective.
