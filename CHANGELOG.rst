Releasenotes
============

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`__,
and this project will adhere to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`__.

We use `towncrier <https://github.com/twisted/towncrier>`__ for the
generation of our release notes file.

Information about unreleased changes can be found
`here <https://gitlab.com/yaook/bare-metal/ansible-playbooks/-/tree/devel/docs/_releasenotes?ref_type=heads>`__.

For changes before winter 2024/25 you only can check for changes using e.g.
``git log --no-merges`` which will give you a rough overview of earlier changes.

.. towncrier release notes start

v1.1.0 (2025-01-30)
-------------------

New Features
~~~~~~~~~~~~

- Add towncrier as changelog creation tool. (`#7 <https://gitlab.com/yaook/bare-metal/ansible-playbooks/-/merge_requests/7>`_)
- We start using a release process! (`#12 <https://gitlab.com/yaook/bare-metal/ansible-playbooks/-/merge_requests/12>`_)


Bugfixes
~~~~~~~~

- Fix some issues and add ReadMe (`#14 <https://gitlab.com/yaook/bare-metal/ansible-playbooks/-/merge_requests/14>`_)
