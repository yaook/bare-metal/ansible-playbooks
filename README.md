# Ansible playbooks for a bare-metal YAOOK installation

This repository contains ansible playbooks to setup an YMC and OpenStack Cluster using
YAOOK and YAOOK/k8s.

## Prepare repository

On the install-node: 

- `git clone https://gitlab.com/yaook/bare-metal/ansible-playbooks.git bm-ansible`
- `cd bm-ansible`
- `cp -r inventory/sample inventory/<deployment-name>`
- Adjust `group_vars/all.yaml` and `hosts` file in `inventory/<deployment-name>`
- Create python venv `python3 -m venv .venv`
- Activate venv `source .venv/bin/activate`
- Install python dependencies `pip install -r requirements.txt`
- Install ansible galaxy collections `ansible-galaxy install -r requirements.yaml`


## Usage

After preparing the repository locally, the playbooks should be executed one after
another with some steps between:

1. **PLAYBOOK:** `ansible-playbook -i inventory/<deployment-name>/hosts setup_install_node.yaml`
1. `cd <cluster-directory>/mgmt-cluster`
1. Run yaook/k8s core role: `./managed-k8s/actions/apply-k8s-core.sh`
1. Untaint all master nodes `kubectl taint nodes --all  node-role.kubernetes.io/control-plane:NoSchedule-`
1. Run yaook/k8s supplements Role `./managed-k8s/actions/apply-k8s-supplements.sh`
1. Add the IPMI Credentials to your environment `export IPMI_USER=<username>`and `export IPMI_PASSWORD=<password>`
1. **PLAYBOOK:** `ansible-playbook -i config/ansible-playbooks/inventory/<deployment-name>/hosts config/ansible-playbooks/setup_ymc_workload.yaml`
1. When prompted to do so, run `./managed-k8s/actions/apply-k8s-supplements.sh install-vault.yaml`
1. **PLAYBOOK:** `ansible-playbook -i config/ansible-playbooks/inventory/<deployment-name>/hosts config/ansible-playbooks/setup_ymc_workload.yaml`
1. From the mgmt-cluster directory, run the provisioning script with `python3 main.py -c <cluster-name> -d <storage-device-type>` (e.g. `python3 main.py -c test.cluster -d hdd`)
    
    *TODO: Where does it come from?*
1. `./managed-k8s/actions/update_inventory.py`
1. `kubectl exec -n yaook-bmc metal-controller-0 -- cat /var/lib/metal-controller/<cluster-name>/inventory/yaook-k8s/hosts` and copy the content to `<cluster-directory>/<cluster-name>/inventory/yaook-k8s/hosts`
1. `cd <cluster-directory>/<cluster-name>`
1. Run `./managed-k8s/actions/apply-k8s-supplements.sh ` (if there is an error, run `./managed-k8s/tools/vault/init.sh` and `./managed-k8s/tools/vault/mkcluster-root.sh`)
1. **PLAYBOOK:** `ansible-playbook -i ../mgmt-cluster/config/ansible-playbooks/inventory/<deployment-name>/hosts -i inventory/yaook-k8s/hosts ../mgmt-cluster/config/ansible-playbooks/setup_openstack_workload.yaml -e "ansible_python_interpreter=python3"`
1. In `config/config.toml` set `rook_enabled = true ` and copy the ceph-config from the statemachine into the `config.toml` ceph part.

    *TODO: What statemachine?*
1. Run `./managed-k8s/actions/apply-k8s-supplements.sh `
1. **PLAYBOOK:** `ansible-playbook -i ../mgmt-cluster/config/ansible-playbooks/inventory/<deployment-name>/hosts -i inventory/yaook-k8s/hosts ../mgmt-cluster/config/ansible-playbooks/setup_openstack_workload.yaml -e "ansible_python_interpreter=python3"`

## Available Playbooks

**The offline mode is currently not tested and therefore not included in the following lists.**

### `setup_install_node.yaml`

This playbook prepares the install node and the yaook/k8s cluster-repository for the YMC.

- Install required APT packages (when `use_install_node` is enabled)
- Clone and configure yaook/k8s cluser-repo for YMC
- Generate CA for YMC
- Push yaook/k8s cluster repo to git (when `git_versioning` is enabled)

### `setup_ymc_workload.yaml`

After the YMC cluster has been successfully installed via yaook/k8s, this playbook
deploys the necessary workload.

- Setup LVM CSI driver
- Configure YMC cert-manager to use generated CA
- Setup vault (yaook/k8s must be run again here)
- Setup and fill netbox
- Install necessary YAOOK operators
- Create necessary YAOOK deployments (keystone, ironic)
- Clone and configure yaook/k8s cluster-repo for OpenStack cluster

### `setup_openstack_workload.yaml`

- Resize root LVM partition of k8s nodes
- Generate CA for OpenStack cluster
- Configure cert-manager to use generated CA
- Setup LVM CSI driver
- Install necessary YAOOK operators
- Create necessary YAOOK deployments
