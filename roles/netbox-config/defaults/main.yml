---
# defaults file for netbox-config
netbox_url: http://netbox.ymc.{{ cluster_name }}:32080/

yaook_custom_choice_sets:
  - name: custome_interfaces
    extra_choices:
      - [802.3ad, 802.3ad]
      - [active-backup, active-backup]
  - name: custom_bond_hash
    extra_choices:
      - [layer3+4, layer3+4]
      - [layer2, layer2]
  - name: custom_bond_lacp_rate
    extra_choices:
      - [slow, slow]
      - [fast, fast]


yaook_custom_fields:
  - name: yaook_hostname_prefix
    type: text
    object_types: dcim.devicerole
    default:
    choice_set:
    validation_regex: ^[a-z]{1}[a-z0-9]{2}$
  - name: yaook_kubernetes_node
    type: boolean
    object_types: dcim.devicerole
    default:
    choice_set:
    validation_regex:
  - name: yaook_nameserver
    type: text
    object_types: ipam.prefix
    default:
    choice_set:
    validation_regex:
  - name: yaook_scheduling_keys
    type: text
    object_types: dcim.devicerole
    default:
    choice_set:
    validation_regex:
      ^([a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*/)?[a-z0-9A-Z]([a-z0-9A-Z._-]{0,61}[a-z0-9A-Z])?(:(true|false|strict))(\s*,\s*([a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*/)?[a-z0-9A-Z]([a-z0-9A-Z._-]{0,61}[a-z0-9A-Z])?(:(true|false|strict)))*$
  - name: yaook_control_plane
    type: boolean
    object_types: dcim.devicerole
    default:
    choice_set:
    validation_regex:
  - name: yaook_dns_label
    type: text
    object_types: ipam.role
    default:
    choice_set:
    validation_regex: ^([a-z][a-z0-9-]*|\\.)$
  - name: yaook_domain
    type: text
    object_types: virtualization.cluster
    default:
    choice_set:
    validation_regex: ^([a-z][a-z0-9-]{0,62}\.)*([a-z][a-z0-9-]{0,62})$
  - name: bond_mode
    type: select
    object_types: dcim.interface
    default: 802.3ad
    choice_set: custome_interfaces
    validation_regex:
  - name: bond_hash
    type: select
    object_types: dcim.interface
    default: layer3+4
    choice_set: custom_bond_hash
    validation_regex:
  - name: bond_lacp_rate
    type: select
    object_types: dcim.interface
    default: slow
    choice_set: custom_bond_lacp_rate
    validation_regex:

yaook_custom_tags:
  - name: yaook--default-gateway
    slug: yaook--default-gateway
    description: Placed on an IP address, it will be used as default gateway for nodes deployed into the corresponding VLAN/Prefix.
  - name: yaook--has-orchestration-error
    slug: yaook--has-orchestration-error
    description: Marked by yaook if errors occur, Messages will be found in comment section of the NetBox device.
  - name: yaook--ip-auto-assign
    slug: yaook--ip-auto-assign
    description: Placed onto a VLAN, it triggers automatic IP address allocation for any interface which is in that VLAN on nodes which get deployed by the metal
      controller; Placed onto a Prefix, it marks that prefix.
  - name: yaook--ip-primary
    slug: yaook--ip-primary
    description: Placed onto a VLAN, it marks this VLAN as the source for the "primary" IP address of a node. This only has an effect on VLANs with yaook--ip-auto-assign
      set.

netbox_clusters:
  - name: "{{ cluster_name }}"
    cluster_type: Yaook
    site: "{{ netbox_site_name }}"
    custom_fields:
      yaook_domain: "{{ cluster_name }}"

netbox_vlans:
  - name: PXE
    vid: 58
    site: "{{ netbox_site_name }}"
  - name: external-0
    vid: 3000
    site: "{{ netbox_site_name }}"
  - name: ipmi-0
    vid: 3001
    site: "{{ netbox_site_name }}"

netbox_vrfs:
  - name: yaook

netbox_prefixes:
  - prefix: 10.2.0.0/20
    status: active
    vrf: yaook
    vlan: external-0
    custom_fields:
      yaook_nameserver: 10.2.0.1
  - prefix: 10.2.16.0/20
    status: active
    vrf: yaook
    vlan: ipmi-0
  - prefix: 10.255.0.0/16
    status: active
    vrf: yaook
    vlan: PXE

netbox_ip_addresses:
  - address: 10.2.0.1/20
    status: active
    vrf: yaook
    tags:
      - yaook--default-gateway
  - address: 10.2.15.1/20
    status: active
    vrf: yaook
    description: The Yaook clusters Kubernetes API VIP

netbox_device_roles:
  - name: Compute
    slug: compute
    custom_fields:
      yaook_control_plane: false
      yaook_hostname_prefix: cmp
      yaook_kubernetes_node: true
  - name: Controller
    slug: controller
    custom_fields:
      yaook_control_plane: true
      yaook_hostname_prefix: ctl
      yaook_kubernetes_node: true
    description: Kubernetes Master node
  - name: Storage
    slug: storage
    custom_fields:
      yaook_control_plane: false
      yaook_hostname_prefix: sto
      yaook_kubernetes_node: true
